 # Clean Architecture Api Template

This is a solution template for creating an API using ASP.NET Core following the principles of Clean Architecture. 


## Technologies
* .NET Core 3.1
* ASP .NET Core 3.1
* Entity Framework Core 3.1
* MediatR
* FluentValidation
* NUnit, FluentAssertions & Moq

### Database Configuration

The template is configured to use an m1 database by default.

This project have a setting flag to use a SQLite database in integration tests:

```json
  "UseTestDatabase": true,
```

Verify that the **Database** connection string within **appsettings.json** points to a valid SQL Server instance. 

### Database Migrations

To use `dotnet-ef` for your migrations please add the following flags to your command (values assume you are executing from repository root)

- `--project src/Infrastructure` (optional if in this folder)
- `--startup-project src/Api`
- `--output-dir Persistence/Migrations`

For example, to add a new migration from the root folder:

 `dotnet ef migrations add "SampleMigration" --project src\Infrastructure --startup-project src\Api --output-dir Persistence\Migrations`

## Overview

### Core

This layer contains all entities, enums, exceptions, interfaces, types and all application logic. This layer defines interfaces that are implemented by outside layers. For example, if the application need to access a notification service, a new interface would be added to application and an implementation would be created within infrastructure.


### Infrastructure

This layer contains classes for accessing external resources such as file systems, web services, smtp, and so on. These classes should be based on interfaces defined within the core layer.

### Api

This layer is a single web api application. This layer depends on both the Application and Infrastructure layers, however, the dependency on Infrastructure is only to support dependency injection. Therefore only *Startup.cs* should reference Infrastructure.