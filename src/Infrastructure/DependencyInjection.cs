using Core.Interfaces;
using Infrastructure.Persistence;
using Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration config)
        {
            if (config.GetValue<bool>("UseTestDatabase"))
            {
                services.AddDbContext<AppDbContext>(options =>
                    options.UseSqlite("Filename=test.db",
                        builder => builder.MigrationsAssembly(typeof(AppDbContext).Assembly.FullName)));
            }
            else
            {
                services.AddDbContext<AppDbContext>(options =>
                    options.UseSqlServer(config.GetConnectionString("Database"))
                );
            }

            services.AddScoped<IAppDbContext>(provider => provider.GetService<AppDbContext>());
            services.AddSingleton(typeof(IEmailService), typeof(EmailService));
            services.AddScoped<IAuthService, AuthService>();

            return services;
        }
    }
}