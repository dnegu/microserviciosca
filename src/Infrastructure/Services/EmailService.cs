﻿using System;
using System.IO;
using System.Linq;
using Core.Interfaces;
using Core.Models;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MimeKit;

namespace Infrastructure.Services
{
    public class EmailService : IEmailService
    {
        private readonly ILogger<EmailService> _logger;
        private readonly IConfiguration _config;
        private readonly IWebHostEnvironment _env;

        public EmailService(ILogger<EmailService> logger, IConfiguration config, IWebHostEnvironment env)
        {
            _logger = logger;
            _config = config;
            _env = env;
        }

        private string BuildEmailBody(string title, string subtitle, string message)
        {
            string body;

            var path = Path.Combine(_env.ContentRootPath, "Templates", "Email", "BasicTemplate.html");

            using (var reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{Title}", title);
            body = body.Replace("{Subtitle}", subtitle);
            body = body.Replace("{Message}", message);
            return body;
        }

        public bool SendEmail(EmailMessageDto emailMessageDto)
        {
            var message = new MimeMessage();

            var from = new MailboxAddress("Dnegu", _config["Email:Username"]);
            message.From.Add(from);

            var recipients = new InternetAddressList();
            foreach (var to in emailMessageDto.Recipients.Select(address => new MailboxAddress("User", address)))
            {
                recipients.Add(to);
            }

            message.To.AddRange(recipients);

            message.Subject = emailMessageDto.Subject;

            var bodyBuilder = new BodyBuilder
            {
                HtmlBody = BuildEmailBody(
                    emailMessageDto.MessageTitle,
                    emailMessageDto.MessageSubtitle,
                    emailMessageDto.MessageBody),
                TextBody = ""
            };

            message.Body = bodyBuilder.ToMessageBody();

            var client = new SmtpClient();
            try
            {
                client.Connect(
                    _config["Email:Host"],
                    int.Parse(_config["Email:Port"]),
                    (bool.Parse(_config["Email:UseSSL"])) ? SecureSocketOptions.StartTls : SecureSocketOptions.None
                );

                if (_config["Email.Password"] != null && _config["Email.Password"] != "")
                {
                    client.Authenticate(_config["Email:Username"], _config["Email:Password"]);
                }

                client.Send(message);
                client.Disconnect(true);
                client.Dispose();
                return true;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, e.Message);
                return false;
            }
        }
    }
}