using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class GenericConfiguration : IEntityTypeConfiguration<Generic>
    {
        public void Configure(EntityTypeBuilder<Generic> builder)
        {
            builder.HasNoKey();
        }
    }
}