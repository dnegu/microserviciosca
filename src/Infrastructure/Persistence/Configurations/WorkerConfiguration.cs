using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class WorkerConfiguration : IEntityTypeConfiguration<Worker>
    {
        public const string TableName = "WORKERS";

        public void Configure(EntityTypeBuilder<Worker> builder)
        {
            builder.ToTable(TableName);

            builder.Property(t => t.Id).HasMaxLength(10).IsRequired().HasColumnName("RUT");

            builder.Property(t => t.Name).HasMaxLength(50).HasColumnName("NOMBRES");

            builder.Property(t => t.Lastname).HasMaxLength(50).HasColumnName("APELLIDOS");
        }
    }
}