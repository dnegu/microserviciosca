﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Persistence.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Generic",
                columns: table => new
                {
                    Result = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "WORKERS",
                columns: table => new
                {
                    RUT = table.Column<string>(maxLength: 10, nullable: false),
                    NOMBRES = table.Column<string>(maxLength: 50, nullable: false),
                    APELLIDOS = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WORKERS", x => x.RUT);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Generic");

            migrationBuilder.DropTable(
                name: "WORKERS");
        }
    }
}
