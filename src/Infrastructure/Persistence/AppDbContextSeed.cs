using System.Linq;
using System.Threading.Tasks;
using Core.Entities;

namespace Infrastructure.Persistence
{
    public class AppDbContextSeed
    {
        public static async Task SeedSampleDataAsync(AppDbContext context)
        {
            // seed if necessary
            if (!context.Worker.Any())
            {
                context.Worker.Add(new Worker
                {
                    Id = "9999999999",
                    Name = "DEFAULT",
                    Lastname = "WORKER"
                });
            }

            await context.SaveChangesAsync();
        }
    }
}