using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Core.Application.App.Queries
{
    public class CrashQuery : IRequest<string>
    {
        public string Message { get; set; }
    }

    public class CrashQueryHandler : IRequestHandler<CrashQuery, string>
    {
        public Task<string> Handle(CrashQuery request, CancellationToken cancellationToken)
        {
            throw new Exception($"{request.Message}");
        }
    }
}