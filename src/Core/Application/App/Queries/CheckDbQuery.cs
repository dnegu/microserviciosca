using System.Threading;
using System.Threading.Tasks;
using Core.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Core.Application.App.Queries
{
    public class CheckDbQuery : IRequest<string>
    {
    }

    public class CheckDbQueryHandler : IRequestHandler<CheckDbQuery, string>
    {
        private readonly IAppDbContext _context;

        public CheckDbQueryHandler(IAppDbContext context)
        {
            _context = context;
        }

        public async Task<string> Handle(CheckDbQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Generic.FromSqlRaw("SELECT 'Success DB connection' AS Result")
                .FirstOrDefaultAsync(cancellationToken: cancellationToken);
            return entity.Result;
        }
    }
}