using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Core.Application.Workers.Queries
{
    public class GetWorkersQuery : IRequest<List<Worker>>
    {
    }

    public class GetWorkersQueryHandler : IRequestHandler<GetWorkersQuery, List<Worker>>
    {
        private readonly IAppDbContext _context;

        public GetWorkersQueryHandler(IAppDbContext context)
        {
            _context = context;
        }

        public async Task<List<Worker>> Handle(GetWorkersQuery request, CancellationToken cancellationToken)
        {
            return await _context.Worker.OrderBy(x => x.Id).Take(100).ToListAsync(cancellationToken);
        }
    }
}