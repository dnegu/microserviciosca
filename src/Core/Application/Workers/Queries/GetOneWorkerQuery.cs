using System.Threading;
using System.Threading.Tasks;
using Core.Entities;
using Core.Exceptions;
using Core.Interfaces;
using MediatR;

namespace Core.Application.Workers.Queries
{
    public class GetOneWorkerQuery : IRequest<Worker>
    {
        public string Id { get; set; }
    }

    public class GetOneWorkerQueryHandler : IRequestHandler<GetOneWorkerQuery, Worker>
    {
        private readonly IAppDbContext _context;

        public GetOneWorkerQueryHandler(IAppDbContext context)
        {
            _context = context;
        }

        public async Task<Worker> Handle(GetOneWorkerQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Worker.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Worker), request.Id);
            }

            return entity;
        }
    }
}