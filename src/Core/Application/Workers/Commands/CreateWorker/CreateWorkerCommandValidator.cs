using System.Threading;
using System.Threading.Tasks;
using Core.Interfaces;
using FluentValidation;

namespace Core.Application.Workers.Commands.CreateWorker
{
    public class CreateWorkerCommandValidator : AbstractValidator<CreateWorkerCommand>
    {
        private readonly IAppDbContext _context;

        public CreateWorkerCommandValidator(IAppDbContext context)
        {
            _context = context;

            RuleFor(v => v.Id)
                .NotEmpty().WithMessage("Id is required.")
                .MaximumLength(10).WithMessage("Id must not exceed 10 characters.")
                .MustAsync(BeUniqueId).WithMessage("The specified Id already exists.");

            RuleFor(v => v.Name)
                .NotEmpty().WithMessage("Name is required.")
                .MaximumLength(50).WithMessage("Name must not exceed 50 characters.");

            RuleFor(v => v.Lastname)
                .NotEmpty().WithMessage("Lastname is required.")
                .MaximumLength(50).WithMessage("Lastname must not exceed 50 characters.");
        }

        private async Task<bool> BeUniqueId(string id, CancellationToken cancellationToken)
        {
            return await _context.Worker.FindAsync(id) == null;
        }
    }
}