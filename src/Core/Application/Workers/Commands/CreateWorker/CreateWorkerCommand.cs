using System.Threading;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using MediatR;

namespace Core.Application.Workers.Commands.CreateWorker
{
    public class CreateWorkerCommand : IRequest<Worker>
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Lastname { get; set; }
    }

    public class CreateWorkerCommandHandler : IRequestHandler<CreateWorkerCommand, Worker>
    {
        private readonly IAppDbContext _context;

        public CreateWorkerCommandHandler(IAppDbContext context)
        {
            _context = context;
        }

        public async Task<Worker> Handle(CreateWorkerCommand request, CancellationToken cancellationToken)
        {
            var entity = new Worker {Id = request.Id, Name = request.Name, Lastname = request.Lastname};

            _context.Worker.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity;
        }
    }
}