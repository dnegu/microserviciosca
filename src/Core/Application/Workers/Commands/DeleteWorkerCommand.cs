using System.Threading;
using System.Threading.Tasks;
using Core.Entities;
using Core.Exceptions;
using Core.Interfaces;
using MediatR;

namespace Core.Application.Workers.Commands
{
    public class DeleteWorkerCommand : IRequest
    {
        public string Id { get; set; }
    }

    public class DeleteWorkerCommandHandler : IRequestHandler<DeleteWorkerCommand>
    {
        private readonly IAppDbContext _context;

        public DeleteWorkerCommandHandler(IAppDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteWorkerCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Worker.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Worker), request.Id);
            }

            _context.Worker.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}