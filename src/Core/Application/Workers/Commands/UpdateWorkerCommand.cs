using System.Threading;
using System.Threading.Tasks;
using Core.Entities;
using Core.Exceptions;
using Core.Interfaces;
using MediatR;

namespace Core.Application.Workers.Commands
{
    public class UpdateWorkerCommand : IRequest
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Lastname { get; set; }
    }

    public class UpdateWorkerCommandHandler : IRequestHandler<UpdateWorkerCommand>
    {
        private readonly IAppDbContext _context;

        public UpdateWorkerCommandHandler(IAppDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdateWorkerCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Worker.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Worker), request.Id);
            }

            entity.Name = request.Name;
            entity.Lastname = request.Lastname;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}