using System.Threading;
using System.Threading.Tasks;
using Core.Entities;
using Core.Exceptions;
using Core.Interfaces;
using MediatR;

namespace Core.Application.Auth.Queries
{
    public class TokenQuery : IRequest<string>
    {
        public string Id { get; set; }
    }

    public class TokenQueryHandler : IRequestHandler<TokenQuery, string>
    {
        private readonly IAppDbContext _context;
        private readonly IAuthService _auth;

        public TokenQueryHandler(IAppDbContext context, IAuthService auth)
        {
            _context = context;
            _auth = auth;
        }

        public async Task<string> Handle(TokenQuery request, CancellationToken cancellationToken)
        {
            var entity = await _context.Worker.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Worker), request.Id);
            }

            return _auth.BuildToken(entity.Id, $"{entity.Name} {entity.Lastname}");
        }
    }
}