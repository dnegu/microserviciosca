﻿using Core.Models;

namespace Core.Interfaces
{
    public interface IEmailService
    {
        bool SendEmail(EmailMessageDto emailMessageDto);
    }
}