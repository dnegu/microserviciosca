using System.Threading;
using System.Threading.Tasks;
using Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Core.Interfaces
{
    public interface IAppDbContext
    {
        DbSet<Generic> Generic { get; set; }
        DbSet<Worker> Worker { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}