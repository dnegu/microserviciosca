namespace Core.Interfaces
{
    public interface IAuthService
    {
        public string BuildToken(string id, string name);
    }
}