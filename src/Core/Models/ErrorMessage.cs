namespace Core.Models
{
    public class ErrorMessage
    {
        public object Parameters { get; set; }

        public object Error { get; set; }
    }
}