using System.Collections.Generic;

namespace Core.Models
{
    public class EmailMessageDto
    {
        public string MessageTitle { get; set; }
        public string MessageSubtitle { get; set; }
        public string MessageBody { get; set; }
        public string Subject { get; set; }
        public List<string> Recipients { get; set; }
    }
}