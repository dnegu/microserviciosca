using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Core.Interfaces;
using Core.Models;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Core.Behaviors
{
    public class ExceptionBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly IConfiguration _config;
        private readonly ILogger<TRequest> _logger;
        private readonly IEmailService _emailService;

        public ExceptionBehavior(IConfiguration config, ILogger<TRequest> logger, IEmailService emailService)
        {
            _config = config;
            _logger = logger;
            _emailService = emailService;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken,
            RequestHandlerDelegate<TResponse> next)
        {
            try
            {
                return await next();
            }
            catch (Exception e)
            {
                var requestName = typeof(TRequest).Name;

                _logger.LogError(e, $"Unhandled Exception for Request {requestName} {request}");

                var errorMessage = new ErrorMessage {Parameters = request, Error = e};

                var emailMessage = new EmailMessageDto
                {
                    MessageTitle = "Problemas de estabilidad del momento",
                    MessageSubtitle = DateTime.Now.ToShortDateString(),
                    MessageBody = JsonConvert.SerializeObject(errorMessage, Formatting.Indented),
                    Subject = "Error Api Template",
                    Recipients = new List<string> {_config["Alerts:Recipient"]}
                };

                _emailService.SendEmail(emailMessage);

                throw;
            }
        }
    }
}