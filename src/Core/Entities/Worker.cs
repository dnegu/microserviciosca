﻿namespace Core.Entities
{
    public class Worker
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Lastname { get; set; }
    }
}