﻿using System.Threading.Tasks;
using Core.Application.Auth.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class AuthController : ApiController
    {
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login([FromBody] TokenQuery query)
        {
            if (query == null) return Unauthorized();

            return Ok(new {Token = await Mediator.Send(query)});
        }
    }
}