﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Application.Workers.Commands;
using Core.Application.Workers.Commands.CreateWorker;
using Core.Application.Workers.Queries;
using Core.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Authorize]
    public class WorkerController : ApiController
    {
        // GET: api/Worker
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Worker>>> GetWorker()
        {
            return await Mediator.Send(new GetWorkersQuery());
        }

        // GET: api/Worker/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Worker>> GetWorker(string id)
        {
            return await Mediator.Send(new GetOneWorkerQuery {Id = id});
        }

        // PUT: api/Worker/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWorker(string id, UpdateWorkerCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        // POST: api/Worker
        [HttpPost]
        public async Task<ActionResult<Worker>> PostWorker(CreateWorkerCommand command)
        {
            return await Mediator.Send(command);
        }

        // DELETE: api/Worker/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Worker>> DeleteWorker(string id)
        {
            await Mediator.Send(new DeleteWorkerCommand {Id = id});

            return NoContent();
        }
    }
}