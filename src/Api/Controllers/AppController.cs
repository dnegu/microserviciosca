﻿using System.Threading.Tasks;
using Core.Application.App.Queries;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class AppController : ApiController
    {
        [HttpGet("/api/info")]
        public IActionResult Get()
        {
            var name = GetType().Assembly.GetName().Name;
            var version = GetType().Assembly.GetName().Version?.ToString();

            return Ok(new {Name = name, Version = version});
        }

        [HttpGet("/api/check")]
        public async Task<string> CheckConnection()
        {
            return await Mediator.Send(new CheckDbQuery());
        }

        [HttpGet("/api/crash")]
        public async Task<string> TestCrash()
        {
            return await Mediator.Send(new CrashQuery {Message = "Throw Api Crash Test"});
        }
    }
}