using System.Threading.Tasks;
using Core.Application.Workers.Commands.CreateWorker;
using Core.Exceptions;
using FluentAssertions;
using NUnit.Framework;

namespace IntegrationTests.Worker.Commands
{
    using static Testing;

    public class CreateWorkerTest : TestBase
    {
        [Test]
        public void ShouldRequireMinimumFields()
        {
            var command = new CreateWorkerCommand();

            FluentActions.Invoking(() => SendAsync(command)).Should().Throw<ValidationException>();
        }

        [Test]
        public async Task ShouldRequireUniqueId()
        {
            await SendAsync(new CreateWorkerCommand
            {
                Id = "46221041",
                Name = "JOHAN",
                Lastname = "GUERREROS"
            });

            var command = new CreateWorkerCommand
            {
                Id = "46221041",
                Name = "JOHAN",
                Lastname = "GUERREROS"
            };

            FluentActions.Invoking(() => SendAsync(command)).Should().Throw<ValidationException>();
        }

        [Test]
        public async Task ShouldCreateWorker()
        {
            var command = new CreateWorkerCommand
            {
                Id = "42817164",
                Name = "CARLOS",
                Lastname = "ALLCA"
            };

            await SendAsync(command);

            var worker = await FindAsync<Core.Entities.Worker>(command.Id);

            worker.Should().NotBeNull();
            worker.Id.Should().Be(command.Id);
            worker.Name.Should().Be(command.Name);
            worker.Lastname.Should().Be(command.Lastname);
        }
    }
}