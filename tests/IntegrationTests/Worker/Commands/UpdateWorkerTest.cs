using System.Threading.Tasks;
using Core.Application.Workers.Commands;
using Core.Application.Workers.Commands.CreateWorker;
using FluentAssertions;
using NUnit.Framework;

namespace IntegrationTests.Worker.Commands
{
    using static Testing;

    public class UpdateWorkerTest : TestBase
    {
        [Test]
        public async Task ShouldUpdateWorker()
        {
            await SendAsync(new CreateWorkerCommand
            {
                Id = "46221041",
                Name = "JOHAN",
                Lastname = "GUERREROS"
            });

            var command = new UpdateWorkerCommand
            {
                Id = "46221041",
                Name = "JESUS",
                Lastname = "PALO"
            };

            await SendAsync(command);

            var worker = await FindAsync<Core.Entities.Worker>(command.Id);

            worker.Name.Should().Be(command.Name);
            worker.Lastname.Should().Be(command.Lastname);
        }
    }
}