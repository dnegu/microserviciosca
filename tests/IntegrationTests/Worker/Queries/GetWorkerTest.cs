using System.Threading.Tasks;
using Core.Application.Workers.Commands;
using Core.Application.Workers.Commands.CreateWorker;
using Core.Application.Workers.Queries;
using Core.Exceptions;
using FluentAssertions;
using NUnit.Framework;

namespace IntegrationTests.Worker.Queries
{
    using static Testing;

    public class GetWorkerTest : TestBase
    {
        [Test]
        public async Task ShouldReturnWorker()
        {
            await SendAsync(new CreateWorkerCommand
            {
                Id = "46221041",
                Name = "JOHAN",
                Lastname = "GUERREROS"
            });

            var query = new GetOneWorkerQuery
            {
                Id = "46221041"
            };

            var result = await SendAsync(query);

            result.Should().NotBeNull();
            result.Id.Should().Be(query.Id);
        }
    }
}